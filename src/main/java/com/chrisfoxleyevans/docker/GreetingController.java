package com.chrisfoxleyevans.docker;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/greeting")
public class GreetingController {

  @GetMapping
  public String getGreeting() {
    return "Hello Spring Boot & Docker!";
  }

  @GetMapping("/{name}")
  public String getCustomisedGreeting(@PathVariable("name") String name) {
    return "Hello " + name + "!";
  }
}
