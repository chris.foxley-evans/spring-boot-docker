# spring-boot-docker

A simple Spring Boot application with a basic docker file 
to act as a working example for a post on medium.com

## Tech

Spring Boot - https://spring.io

Maven - https://maven.apache.org/

Docker - https://www.docker.com/

## Build instructions

The application is managed with maven to simplify dependency management and build scripts

### Application
```shell script
mvn clean package
```

### Container image

```shell script
docker build -t {reponame}/{imagename} .
docker run -p 8080:8080 {reponame}/{imagename}
```